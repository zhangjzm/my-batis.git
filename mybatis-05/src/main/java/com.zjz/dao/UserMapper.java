package com.zjz.dao;

import com.zjz.pojo.User;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

public interface UserMapper {

    @Select("select * from user")
    List<User> GetUsers();

    // 方法存在多个参数，所有参数前面必须加@Param("X")注解
    @Select("select * from user where id =#{id} AND name = #{name}")
    List<User> getUserByIdName(@Param("id")int id,@Param("name")String name);

    @Insert("insert into user(id,name,password) values(#{id},#{name},#{password})")
    int addUser(User user);

    @Update("update user set name=#{name},password=#{password} where id=#{id}")
    int updateUser(User user);


    @Delete("delete from user where id=#{id}")
    int deleteUser(@Param("id")int id);

}
