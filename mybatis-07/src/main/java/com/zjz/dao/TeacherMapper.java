package com.zjz.dao;

import com.zjz.pojo.Teacher;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface  TeacherMapper {

     // 获取老师
    List<Teacher> getTeacher();

    // 获取指定老师下的所有学生信息

    Teacher getTeacherS(@Param("tid")int id);

    Teacher getTeacherS1(@Param("id")int id);



}
