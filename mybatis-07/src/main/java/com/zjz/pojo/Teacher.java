package com.zjz.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Teacher {
    private int id;
    private String name;

    // 一个老师对应多个学生
    public List<Student> studentList;
}
