import com.zjz.dao.TeacherMapper;
import com.zjz.pojo.Teacher;
import com.zjz.utils.MybatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import java.util.List;

public class Test {

    static Logger logger = Logger.getLogger(Test.class);

    @org.junit.Test
    public void Test1(){
        logger.info("access Test1");
        SqlSession sqlSession = MybatisUtils.getSqlSession();

        TeacherMapper mapper = sqlSession.getMapper(TeacherMapper.class);

        logger.info("getTeacher begin:");
        List<Teacher> teacher = mapper.getTeacher();
        System.out.println(teacher.toString());
        logger.info("getTeacher over:");


        sqlSession.close();
    }


    @org.junit.Test
    public void Test2(){
        logger.info("access Test1");
        SqlSession sqlSession = MybatisUtils.getSqlSession();

        TeacherMapper mapper = sqlSession.getMapper(TeacherMapper.class);

        logger.info("getTeacherS begin:");
        Teacher teacher = mapper.getTeacherS(1);
        System.out.println(teacher.toString());
        logger.info("getTeacherS over:");


        sqlSession.close();
    }


    @org.junit.Test
    public void Test3(){
        logger.info("access Test1");
        SqlSession sqlSession = MybatisUtils.getSqlSession();

        TeacherMapper mapper = sqlSession.getMapper(TeacherMapper.class);

        logger.info("getTeacherS begin:");
        Teacher teacher = mapper.getTeacherS1(2);
        System.out.println(teacher.toString());
        logger.info("getTeacherS over:");


        sqlSession.close();
    }

}
