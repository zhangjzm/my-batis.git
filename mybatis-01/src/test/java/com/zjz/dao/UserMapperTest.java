package com.zjz.dao;

import com.zjz.pojo.User;
import com.zjz.utils.MybatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserMapperTest {
    @Test
    public void test(){

        SqlSession sqlSession = null;
        try {

        // 第一步，获得SqlSession对象
        sqlSession = MybatisUtils.getSqlSession();
        // 执行sql
        // 第一种方式：getMapper 推荐---
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        List<Object> userList = mapper.getUserList();



        System.out.println(userList.toString());
            // 第二种方式;;; 通过sqlSession的方法对绝对位置的mapper方法封装---不推荐，快淘汰了
/*       List<User> list = sqlSession.selectList("com.zjz.dao.UserMapper.getUserList"); // 好像在哪里见了？
        System.out.println(list.toString());*/
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            // 关闭SqlSession
            sqlSession.close();
        }


    }

    @Test
    public void TestGetUserLike(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();

        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        List<User> list = mapper.getUserLike("%zjz%");

        System.out.println(list.toString());

        sqlSession.close();
    }



    @Test
    public void TestGetUserById(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();

        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        User userById = mapper.getUserById(1);
        System.out.println(userById);

        sqlSession.close();
    }


    @Test
    public void TestGetUserById1(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);

        Map<String, Object> map = new HashMap<>();
        map.put("id","8");
/*        map.put("userName","zjz7");
        map.put("userPassword","3241616");*/
        User userById1 = mapper.getUserById1(map);
        System.out.println(userById1);

        sqlSession.close();

    }



    @Test
    public void TestGetUserByIdName(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();

        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        User userById = mapper.getUserByIdName(1,"zjz1");
        System.out.println(userById);

        sqlSession.close();
    }




    @Test
    public void TestAddUser(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);

         mapper.addUser(new User(4, "zjz4", "123456"));

        // 提交事务
        sqlSession.commit();

        sqlSession.close();

    }

    @Test
    public void TestAddUser1(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);

        Map<String, Object> map = new HashMap<>();
        map.put("userId","8");
        map.put("userName","zjz7");
        map.put("userPassword","3241616");
        mapper.addUser1(map);

        // 提交事务
        sqlSession.commit();

        sqlSession.close();

    }


    @Test
    public void TestUpdateUser(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);

        mapper.updateUser(new User(4,"zjz04","123456"));

        sqlSession.commit();
        sqlSession.close();

    }



    @Test
    public void TestDeleteUser(){

        SqlSession sqlSession = MybatisUtils.getSqlSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);

        mapper.deleteUser(7);

        sqlSession.commit();
        sqlSession.close();
    }



}
