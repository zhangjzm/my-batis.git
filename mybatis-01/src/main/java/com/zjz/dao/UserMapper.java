package com.zjz.dao;

import com.zjz.pojo.User;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface UserMapper {
    // 查询全部用户
    List<Object> getUserList(); // list中可以指定POJO，也可以指定Object（多表）


    // 模糊查询
    List<User> getUserLike(String name);

    // 依据id，name查询
    User getUserByIdName(@Param("id") int id,@Param("name") String name);
    // 根据id查询
    User getUserById(int id);
//    User getUserById1(@Param("id")int id); // 推荐用法
    User  getUserById1(Map<String,Object> map);  // map 用法

    // 插入insert
    int addUser(User user);

    // map的使用---万能的Map
    int addUser1(Map<String,Object> map);



    // 修改用户
    int updateUser(User user);

    // 删除用户
    int deleteUser(int id);
}
