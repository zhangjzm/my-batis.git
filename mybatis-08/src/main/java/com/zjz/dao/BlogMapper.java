package com.zjz.dao;

import com.zjz.pojo.Blog;

import java.util.List;
import java.util.Map;

public interface BlogMapper {
    // 插入数据
    int addBook(Blog blog);

    List<Blog> QueryALL();

    // 查询博客IF
    List<Blog> QueryBlogIF(Map map);


    // 查询博客Choose
    List<Blog> QueryBlogChoose(Map map);


    // 更新博客
    int updateBlog(Map map);


    // 查询 id 1-5的博客
    List<Blog> QueryBlogForeach(Map map);


}
