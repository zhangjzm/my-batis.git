import com.zjz.dao.BlogMapper;
import com.zjz.pojo.Blog;
import com.zjz.utils.IDUtils;
import com.zjz.utils.MybatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class Mytest {
    Logger logger = Logger.getLogger(Mytest.class);
    @Test
    public void Test1(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();

        BlogMapper mapper = sqlSession.getMapper(BlogMapper.class);

        logger.info("添加开始");
        Blog blog = new Blog(IDUtils.getID(),"Mysbatis1","zjz",(new Date()),888);
        Blog blog1 = new Blog(IDUtils.getID(),"Mysbatis2","zjz1",(new Date()),999);

        Blog blog2 = new Blog();
        blog2.setId(IDUtils.getID());
        blog2.setAuthor("zjz2");
        blog2.setTitle("我得学习");
        Date dat = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
        String format = simpleDateFormat.format(dat);
        System.out.println(format);
        logger.info("format" +format);
        blog2.setCreateTime(dat);
//        mapper.addBook(blog);
//        mapper.addBook(blog1);
//        mapper.addBook(blog2);





        mapper.addBook(new Blog(IDUtils.getID(),"MYTitle1","author1",(new Date()),10000));
        mapper.addBook(new Blog(IDUtils.getID(),"MYTitle2","author2",(new Date()),10000));
        mapper.addBook(new Blog(IDUtils.getID(),"MYTitle3","author3",(new Date()),10000));
        mapper.addBook(new Blog(IDUtils.getID(),"MYTitle4","author4",(new Date()),10000));
        mapper.addBook(new Blog(IDUtils.getID(),"MYTitle5","author5",(new Date()),10000));
        mapper.addBook(new Blog(IDUtils.getID(),"MYTitle6","author6",(new Date()),10000));
        mapper.addBook(new Blog(IDUtils.getID(),"MYTitle7","author7",(new Date()),10000));




         (new Mytest()).Test2();




        logger.info("添加结束");
        sqlSession.close();
    }



    @Test
    public void Test2(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();

        BlogMapper mapper = sqlSession.getMapper(BlogMapper.class);

        logger.info("查询开始");

        List<Blog> blogs = mapper.QueryALL();
        System.out.println(blogs.toString());

        logger.info("查询结束");
        sqlSession.close();
    }



    @Test
    public void Test3(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();

        BlogMapper mapper = sqlSession.getMapper(BlogMapper.class);
        logger.info("QueryBlogIF开始");

        HashMap map = new HashMap();



        map.put("title","MYTitle1");
        map.put("author","author1");
        List<Blog> blogs = mapper.QueryBlogIF(map);


        for (Blog blog : blogs) {
            System.out.print(blog);

        }
        System.out.println();

        logger.info("QueryBlogIF结束");
        sqlSession.close();
    }





    @Test
    public void Test4(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();

        BlogMapper mapper = sqlSession.getMapper(BlogMapper.class);
        logger.info("QueryBlogChoose开始");

        HashMap map = new HashMap();

//        map.put("title","MYTitle1");
//        map.put("author","author1");
        map.put("views","10000");
        List<Blog> blogs = mapper.QueryBlogChoose(map);


        for (Blog blog : blogs) {
            System.out.println(blog);
        }
        System.out.println();

        logger.info("QueryBlogChooseF结束");
        sqlSession.close();
    }



    @Test
    public void Test5(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();

        BlogMapper mapper = sqlSession.getMapper(BlogMapper.class);
        logger.info("updateBlog开始");

        HashMap map = new HashMap();

//        map.put("title","updateMYTitle1");
        map.put("author","updateauthor2");

        map.put("id","b93fdb882cfc442ba41dbd3f3365f354");
        int i = mapper.updateBlog(map);



        logger.info("updateBlog结束");
        sqlSession.close();
    }



    @Test
    public void TestQueryBlogForeach(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();

        BlogMapper mapper = sqlSession.getMapper(BlogMapper.class);
        logger.info("QueryBlogForeach开始");

        HashMap map = new HashMap();

        List<Integer> ids = new ArrayList<>();

        ids.add(1);
        ids.add(2);
        map.put("ids",ids);

        List<Blog> blogs = mapper.QueryBlogForeach(map);

        System.out.println(blogs.toString());

        logger.info("QueryBlogForeach结束");
        sqlSession.close();
    }



}
