import com.dao.StudentMapper;
import com.dao.TeacherMapper;
import com.pojo.Student;
import com.pojo.Teacher;
import com.utils.MybatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import java.util.List;

public class Test {


    static Logger logger = Logger.getLogger(Test.class);

    @org.junit.Test
    public void Test(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        TeacherMapper mapper = sqlSession.getMapper(TeacherMapper.class);
        logger.info("Test start:");
        Teacher teacher = mapper.getTeacher(1);

        System.out.println(teacher.toString());

        logger.info("Test end:");

        sqlSession.close();


    }



    @org.junit.Test
    public void Test1(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        StudentMapper mapper = sqlSession.getMapper(StudentMapper.class);
        logger.info("Test start:");

        List<Student> studentList = mapper.getStudentList();

        System.out.println(studentList.toString());

        logger.info("Test end:");

        sqlSession.close();


    }



    @org.junit.Test
    public void Test2(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        StudentMapper mapper = sqlSession.getMapper(StudentMapper.class);
        logger.info("Test start:");

        List<Student> studentList = mapper.getStudentList2();

        System.out.println(studentList.toString());

        logger.info("Test end:");

        sqlSession.close();


    }


}
