package com.zjz.dao;

import com.zjz.pojo.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserMapper {

    List<User> queryUsers();


    // 根据ID查询指定用户
    User quertUserById(@Param("id")int id);

    // 更新用户
    int updateUser(User user);



}
