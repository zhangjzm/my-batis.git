import com.zjz.dao.UserMapper;
import com.zjz.pojo.User;
import com.zjz.utils.MybatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

public class MyTest {


    // 二级cache Test
    @Test
    public void TestCache(){


        SqlSession sqlSession = MybatisUtils.getSqlSession();
        SqlSession sqlSession1 = MybatisUtils.getSqlSession();


        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        UserMapper mapper1 = sqlSession1.getMapper(UserMapper.class);

        User user = mapper.quertUserById(1);
        if(user!=null){
            System.out.println(user.toString());
        }
        sqlSession.close();

        User user1 = mapper1.quertUserById(1);
        if(user1!=null){
            System.out.println(user1.toString());

        }

        if(user!=null&&user1!=null){
            /*二级缓存未开启时时false 开启时时true*/
            System.out.println("user=user1----"+(user==user1));
        }




        sqlSession.close();
    }



    @Test
    public void quertUserByIdTest(){

        SqlSession sqlSession = MybatisUtils.getSqlSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        User user1 = mapper.quertUserById(1);

        /* 清理缓存 clearCache*/
        sqlSession.clearCache();
        // 测试从哪里取值
        User user2 = mapper.quertUserById(1);

        if(user1 != null && user2 != null){
            System.out.println(user1.toString());       // User(id=1, name=zjz1, password=123456)
            System.out.println(user2.toString());       // User(id=1, name=zjz1, password=123456)
            System.out.println(user1==user2);       // true
        }else {
            System.out.println("用户不存在！");
        }
        sqlSession.close();
    }


    @Test
    public void updateUserTest(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);

        mapper.updateUser(new User(5,"zhangsan","188741515215"));

        sqlSession.commit();
        sqlSession.close();
    }




    @Test
    public void quertUserByIdTest1(){

        SqlSession sqlSession = MybatisUtils.getSqlSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        User user1 = mapper.quertUserById(1);
        // 测试从哪里取值
        int i = mapper.updateUser(new User(5, "zhangsan", "188741515215"));
        sqlSession.commit();
        User user2 = mapper.quertUserById(1);

        if(user1 != null && user2 != null){
            System.out.println(user1.toString());       // User(id=1, name=zjz1, password=123456)
            if(i>0){
                System.out.println("更新完成！");
            }else{
                System.out.println("更新操作出事了！");
            }
            System.out.println(user2.toString());       // User(id=1, name=zjz1, password=123456)
            System.out.println(user1==user2);       // true
        }else {
            System.out.println("用户不存在！");
        }
        sqlSession.close();
    }






}
