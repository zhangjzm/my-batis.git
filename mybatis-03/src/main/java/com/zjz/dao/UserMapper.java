package com.zjz.dao;

import com.zjz.pojo.User;

import java.util.List;
import java.util.Map;

public interface UserMapper {
    // 查询全部用户
    List<Object> getUserList(); // list中可以指定POJO


    // 分页查询
    List<User> getUserLimit(Map<String,Object> map);




    // 根据id查询
    User getUserById(int id);

    // 插入insert
    int addUser(User user);


    // 修改用户
    int updateUser(User user);

    // 删除用户
    int deleteUser(int id);
}
