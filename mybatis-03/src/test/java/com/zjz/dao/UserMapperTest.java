package com.zjz.dao;

import com.zjz.pojo.User;
import com.zjz.utils.MybatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class UserMapperTest {

    //注意导包：org.apache.log4j.Logger
    static Logger logger = Logger.getLogger(UserMapperTest.class); //对应目前的类
    @Test
    public void getUserListLog4j() {
        logger.info("info：进入getUserListLog4j方法");   // 提示信息
        logger.debug("debug：进入getUserListLog4j方法"); // debug信息
        logger.error("error: 进入getUserListLog4j方法"); // 错误信息
        SqlSession session = MybatisUtils.getSqlSession();

        UserMapper mapper = session.getMapper(UserMapper.class);
        List<Object> users = mapper.getUserList();
        System.out.println(users.toString());
        session.close();

    }




    @Test
    public void testGetUserLimit(){

        logger.info("info：进入testGetUserLimit方法");
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        Map<String, Object> map = new HashMap<>();

        map.put("startIndex",0);
        map.put("pageSize",1);

        List<User> userLimit = mapper.getUserLimit(map);
        System.out.println(userLimit.toString());
        sqlSession.close();

    }






    @Test
    public void test(){

        SqlSession sqlSession = null;
        try {

        // 第一步，获得SqlSession对象
        sqlSession = MybatisUtils.getSqlSession();
        // 执行sql
        // 第一种方式：getMapper 推荐---
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        List<Object> userList = mapper.getUserList();

        System.out.println(userList.toString());
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            // 关闭SqlSession
            sqlSession.close();
        }


    }




    @Test
    public void TestGetUserById(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();

        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        User userById = mapper.getUserById(1);
        System.out.println(userById);

        sqlSession.close();
    }





    @Test
    public void TestAddUser(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);

         mapper.addUser(new User(4, "zjz4", "123456"));

        // 提交事务
        sqlSession.commit();

        sqlSession.close();

    }


    @Test
    public void TestUpdateUser(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);

        mapper.updateUser(new User(4,"zjz04","123456"));

        sqlSession.commit();
        sqlSession.close();

    }



    @Test
    public void TestDeleteUser(){

        SqlSession sqlSession = MybatisUtils.getSqlSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);

        mapper.deleteUser(7);

        sqlSession.commit();
        sqlSession.close();
    }



}
