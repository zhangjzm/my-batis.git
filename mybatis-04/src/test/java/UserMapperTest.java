import com.zjz.dao.UserMapper;
import com.zjz.pojo.User;
import com.zjz.utils.MybatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.junit.Test;

import java.util.List;

public class UserMapperTest {

    static Logger logger = Logger.getLogger(UserMapperTest.class); //对应目前的类
    @Test
    public void test(){

        SqlSession sqlSession = MybatisUtils.getSqlSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        logger.info("GetUsers方法开始：：：");
        List<User> userList = mapper.GetUsers();
        logger.info("GetUsers方法结束：：：");
        System.out.println(userList.toString());


        sqlSession.close();
    }


    @Test
    public void testGetUserByIdName(){

        SqlSession sqlSession = MybatisUtils.getSqlSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);

        List<User> userList = mapper.getUserByIdName(1,"zjz1");

        System.out.println(userList.toString());


        sqlSession.close();
    }

    @Test
    public void testAddUser(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);


        mapper.addUser(new User(3,"zjz3","123456"));


        sqlSession.close();

    }


    @Test
    public void testUpdateUser(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);


        mapper.updateUser(new User(4,"zjz4","123456"));


        sqlSession.close();

    }

    @Test
    public void testDeleteUser(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);


        mapper.deleteUser(7);


        sqlSession.close();

    }




}
